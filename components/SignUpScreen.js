import React, {Component} from 'react';
import {
  Button,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
export class SignUpScreen extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#fff',
        }}>
        <Text>Zomato</Text>
        <Button
          onPress={() => this.props.navigation.goBack()}
          title="Go to Login screen"
        />
      </View>
    );
  }
}

export default SignUpScreen;
