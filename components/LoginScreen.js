import React, {Component} from 'react';
import {
  Button,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
export class LoginScreen extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#fff',
        }}>
        <Text>Login Screen</Text>
        <Button
          onPress={() => this.props.navigation.navigate('SignUp')}
          title="Go to SignUp"
        />
      </View>
    );
  }
}

export default LoginScreen;
